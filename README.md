## FastAPI project

System settings: Linux Debian 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64 GNU/Linux

## 1. Configurate Database 

Enter code in bash: 
```bash
sudo -u postgres psql 
```

In postgresql console: 
```psql
CREATE ROLE "your_user" WITH LOGIN PASSWORD 'your_password';
```
Get permission on database creation
```psql
ALTER ROLE "fastapi" CREATEDB; 
```
Create database 
```psql 
CREATE DATABASE fastapi WITH OWNER fastapi;
```

## 2. Create .env file in project directory and write:
```env 
APP_TITLE="YOUR APP TITLE"
APP_VERSION=YOUR_APP_VERSION
HOST=YOUR_APP_HOST
PORT=YOUR_APP_PORT

DB_HOST=YOUR_DB_HOST
DB_USER=YOUR_DB_USER
DB_PASS=YOUR_DB_PASS
DB_NAME=YOUR_DB_NAME
DB_PORT=YOUR_DB_PORT

SECRET_AUTH=YOUR_SECRET_AUTH
SECRET_MANAGER=YOUR_SECRET_MANAGER
```

Secret keys can be generated using ```mkpasswd```

## 3. Run migrations
```bash
alembic upgrade head
```