from dotenv import load_dotenv
from pathlib import Path
import os

dir_path = (Path(__file__) / ".." / "..").resolve()
env_path = os.path.join(dir_path, ".env")
load_dotenv(dotenv_path=env_path)


class Secret:
    SECRET_AUTH: str = os.getenv("SECRET_AUTH")
    SECRET_MANAGER: str = os.getenv("SECRET_MANAGER")


class Settings:
    APP_TITLE: str = os.getenv("APP_TITLE")
    APP_VERSION: int = os.getenv("APP_VERSION")
    HOST: str = os.getenv("HOST")
    PORT: int = os.getenv("PORT")


class DataBaseSettings:
    DB_USER: str = os.getenv("DB_USER")
    DB_PASS: str = os.getenv("DB_PASS")
    DB_NAME: str = os.getenv("DB_NAME")
    DB_PORT: int = os.getenv("DB_PORT")
    DB_HOST: str = os.getenv("DB_HOST")


setting = Settings()
database_settings = DataBaseSettings()
secret = Secret()
