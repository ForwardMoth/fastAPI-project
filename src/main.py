import uvicorn
from fastapi import FastAPI
from src.app.routes.route import router as app_router
from src.config import setting

from fastapi_users import FastAPIUsers

# from src.auth.provides.database import User
# from src.auth.schemas.schemas import UserRead, UserCreate
# from src.auth.manager.manager import get_user_manager
# from src.auth.config.config import auth_backend

app = FastAPI()


def include_router(app):
    app.include_router(app_router)


def start_application():
    app = FastAPI(title=setting.APP_TITLE, version=setting.APP_VERSION)
    include_router(app)
    return app


if __name__ == "__main__":
    app = start_application()
    uvicorn.run(app, host=setting.HOST, port=int(setting.PORT))
