from fastapi_users.authentication import BearerTransport
from fastapi_users.authentication import JWTStrategy
from src.config import secret

bearer_transport = BearerTransport(tokenUrl="auth/jwt/login")

SECRET = secret.SECRET_AUTH


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=SECRET, lifetime_seconds=3600)

