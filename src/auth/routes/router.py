from fastapi_users.authentication import AuthenticationBackend
from src.auth.config.config import bearer_transport, get_jwt_strategy
from fastapi_users import FastAPIUsers
from src.auth.models.users import User
from src.auth.manager.manager import get_user_manager


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=bearer_transport,
    get_strategy=get_jwt_strategy,
)

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

current_user = fastapi_users.current_user()
