from typing import AsyncGenerator
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from src.config import database_settings

DATABASE_URL = f"postgresql+asyncpg://{database_settings.DB_USER}:{database_settings.DB_PASS}" \
               f"@{database_settings.DB_HOST}:{database_settings.DB_PORT}/{database_settings.DB_NAME}"
Base = declarative_base()


engine = create_async_engine(DATABASE_URL)
async_session_maker = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session
