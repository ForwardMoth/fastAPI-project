from sqlalchemy import MetaData, Table, Column, Integer, Float, ForeignKey, Date
from src.auth.models.user import user

metadata = MetaData()

staff = Table(
    "staff",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("salary", Float, nullable=False),
    Column("date_next_increase_salary", Date, nullable=False),
    Column("user_id", Integer, ForeignKey(user.c.id))
)
