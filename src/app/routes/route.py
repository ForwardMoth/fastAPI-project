from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, insert
from src.database import get_async_session
from src.app.models.staff import staff
from src.app.schemas.staff import Staff

# from src.app.controllers.user import User
# from src.app.controllers.staff import Staff


router = APIRouter(
    prefix="/staffs",
    tags=["Staff"]
)

@router.get("/{staff_id}")
async def get_staff(staff_id: int, session: AsyncSession = Depends(get_async_session)):
    try:
        query = select(staff).where(staff.c.id == staff_id)
        result = await session.execute(query)
        return {"status": "success",
                "data": result.all()
                }
    except Exception:
        return {"status": "error", "details": Exception}

@router.post("/")
async def add_staff(new_staff: Staff, session: AsyncSession = Depends(get_async_session)):
    try:
        query = insert(staff).values(**new_staff.dict())
        await session.execute(query)
        await session.commit()
        return {"status": "success"}
    except Exception:
        return {"status": "error", "details": Exception}


@router.get("/")
async def get_all_staffs(session: AsyncSession = Depends(get_async_session)):
    try:
        query = select(staff)
        result = await session.execute(query)
        return {"status": "success", "data": result.all()}
    except Exception:
        return {"status": "error", "details": Exception}

