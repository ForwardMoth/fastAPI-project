from pydantic import BaseModel
from datetime import date


class Staff(BaseModel):
    salary: float
    date_next_increase_salary: date
